from math import ceil
from pathlib import Path


def ler_casos(arquivo: Path):
    stream = arquivo.open('r')

    flag = ''

    casos = []

    for linha in stream:
        if 'C JMARTI SETUP' in linha:
            modais = []

        if 'C BLANK CARD ENDING FREQUENCY CARDS' in linha:
            comp_lt = float(linha_anterior[46:54])
            continue

        linha_anterior = linha

        if '-1' in linha[:2]:
            for n in range(3):
                modais.append({'Zc': [], 'A': []})
                for m in range(2):
                    linha1 = stream.readline()
                    n_poles = int(linha1.split()[0])
                    termo = complex(linha1.split()[1])

                    k = []
                    p = []

                    for l in range(ceil(n_poles / 3)):
                        k += [float(t) for t in stream.readline().split()]

                    for l in range(ceil(n_poles / 3)):
                        p += [float(t) for t in stream.readline().split()]

                    if m == 0:
                        modais[-1]['Zc'].append(termo)
                        modais[-1]['Zc'].append(k)
                        modais[-1]['Zc'].append(p)
                        continue

                    modais[-1]['A'].append(termo)
                    modais[-1]['A'].append(k)
                    modais[-1]['A'].append(p)

                stream.readline()


            flag = ''

            casos.append(modais)

    return casos
