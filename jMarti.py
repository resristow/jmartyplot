# if __package__ is None:
#     __package__ = ''

from numpy import arange, exp, zeros, pi, errstate, abs
from matplotlib import pyplot as plt
from pathlib import Path
from services import ler_casos

casos_dados = ler_casos(Path('Distancia.txt'))

figura_zc = plt.figure(1)
figura_zc.suptitle('Impedancia de surto')
ax_zc = figura_zc.subplots(1, len(casos_dados[0]), sharex=True)

figura_a = plt.figure(2)
figura_a.suptitle('Matriz de Pesos A')
ax_a = figura_a.subplots(1, len(casos_dados[0]), sharex=True)

for dados in casos_dados:

    for m in range(len(dados)):

        modal = dados[m]

        # Calculo do Zc

        f = arange(0, 1e6, 10)
        w = 2 * pi * f

        Z = zeros(len(w)) + modal['Zc'][0]

        with errstate(over='raise'):

            for coef in range(len(modal['Zc'][1])):
                try:
                    Z += modal['Zc'][1][coef] / (1j * w + modal['Zc'][2][coef])
                except FloatingPointError:
                    print('erroooo')

        ax_zc[m].plot(f, abs(Z))
        ax_zc[m].grid()
        ax_zc[m].set_xscale('log')

        # Calculo do A

        timestep = 1e-8

        tau_min = modal['A'][0]

        f = arange(0, 1e6, 10)
        w = 2 * pi * f
        t = arange(tau_min.real - 10 * timestep, 2e-3, timestep)

        A = zeros(len(w), dtype='complex128')
        a = zeros(len(t))

        with errstate(over='raise'):

            for coef in range(len(modal['A'][1])):
                A += modal['A'][1][coef] / (1j * w + modal['A'][2][coef])
                a += modal['A'][1][coef] * exp(-modal['A'][2][coef] * t)

            A = A * exp(-1j * w * tau_min)

        ax_a[m].plot(f, abs(A))
        ax_a[m].grid()
        ax_a[m].set_xscale('log')

plt.show()
